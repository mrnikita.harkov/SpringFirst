package com.example.newTry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewTryApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewTryApplication.class, args);
	}

}
