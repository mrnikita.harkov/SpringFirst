package com.example.newTry.domain.util;

import com.example.newTry.domain.User;

public abstract class MessageHelper {
    public static String getAuthorName(User author){
        return author!=null ? author.getUsername():"<none>";
    }
}
