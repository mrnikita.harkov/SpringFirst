package com.example.newTry.controller;

import com.example.newTry.domain.Message;
import com.example.newTry.domain.User;
import com.example.newTry.domain.dto.MessageDto;
import com.example.newTry.repos.MessageRepo;
import com.example.newTry.service.MessageService;
import com.example.newTry.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequestMapping("/user-messages")
public class MessageController {

    @Autowired
    private MessageRepo messageRepo;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @GetMapping("/{author}")
    public String userMessages(
            @AuthenticationPrincipal User currentUser,
            @PathVariable User author,
            Model model,
            @RequestParam (required = false) Message message,
            @PageableDefault(sort = {"id"},direction = Sort.Direction.DESC)Pageable pageable
            ){
        Page<MessageDto> page = messageService.messageListForUser(pageable,currentUser,author);

        model.addAttribute("userChannel",author);
        model.addAttribute("subscriptionsCount",author.getSubscriptions().size());
        model.addAttribute("subscribersCount",author.getSubscribers().size());
        model.addAttribute("isSubscriber", author.getSubscribers().contains(currentUser));
        model.addAttribute("page", page);
        model.addAttribute("message", message);
        model.addAttribute("isCurrentUser", currentUser.equals(author));
        model.addAttribute("url", "/user-messages/"+author.getId());
        return "userMessages";
    }

    @PostMapping("/{user}")
    public String updateMessage(
            @AuthenticationPrincipal User currentUser,
            @PathVariable Long user,
            @RequestParam("id") Message message,
            @RequestParam("text")String text,
            @RequestParam("tag") String tag,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        if(message.getAuthor().equals(currentUser)){
            if(!StringUtils.isEmpty(text)){
                message.setText(text);
            }
            if(!StringUtils.isEmpty(tag)){
                message.setTag(tag);
            }
            messageService.saveFile(message,file);
            messageRepo.save(message);
        }
        return "redirect:/user-messages/"+user;
    }

}
